<?php
require_once './vendor/autoload.php';
header('Content-Type: text/html; charset=utf-8');

$ail = new \Kangcg\Payment\Vendor\Ali([
    'app_id' => '',
    'public_key' => '',
    'private_key' => '',

    'app_cert' => '',
    'alipay_cert' => '',
]);

//当面付（条码支付)
$ail->unified([
    'method' => 'alipay.trade.pay',
    'out_trade_no' => '',       //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'scene' => 'bar_code',      //条码支付固定传入bar_code
    'auth_code' => '',         //用户付款码，25~30开头的长度为16~24位的数字，实际字符串长度以开发者获取的付款码长度为准
    'total_amount' => '',      //单位 元
    'subject' => '会员',       //订单标题
    'store_id' => '',          //商户门店编号
    'timeout_express' => '2m',  //交易超时时间

    'notify_url' => 'http://baidu.com',  //异步通知接口
]);
//当面付（扫码支付)
$ail->unified([
    'method' => 'alipay.trade.precreate',
    'out_trade_no' => '',       //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'subject' => '会员',       //订单标题
    'total_amount' => '',      //单位 元
    'timeout_express' => '2h',  //交易超时时间
    'notify_url' => 'http://baidu.com',  //异步通知接口
]);

//小程序支付 JSAPI
$ail->unified([
    'method' => 'alipay.trade.create',
    'out_trade_no' => '',   //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'total_amount' => '',   //单位 元
    'subject' => '会员',    //订单标题
    'product_code' => 'JSAPI_PAY', //产品码。商家和支付宝签约的产品码。小程序场景支付：JSAPI_PAY
    'op_app_id' => '', //小程序支付中，商户实际经营主体的小程序应用的appid，也即最终唤起收银台支付所在的小程序的应用id

    'buyer_id' => 'http://baidu.com',  //买家支付宝用户ID 与 buyer_open_id 二选一
    'buyer_open_id' => 'http://baidu.com',  //买家支付宝用户唯一标识 openid简介

    'notify_url' => 'http://baidu.com',  //异步通知接口
]);

//App支付
$ail->unified([
    'method' => 'alipay.trade.app.pay',
    'out_trade_no' => '',   //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'total_amount' => '',   //单位 元
    'subject' => '会员',    //订单标题

    'notify_url' => 'http://baidu.com',  //异步通知接口
]);

//PC网站支付
$ail->unified([
    'method' => 'alipay.trade.page.pay',

    'out_trade_no' => '',   //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'total_amount' => '',   //单位 元
    'subject' => '会员',    //订单标题
    'product_code' => 'FAST_INSTANT_TRADE_PAY',    //销售产品码，与支付宝签约的产品码名称。注：目前电脑支付场景下仅支持FAST_INSTANT_TRADE_PAY

    'notify_url' => 'http://baidu.com',  //异步通知接口
    'return_url' => 'http://baidu.com',  //同步通知接口
]);

//手机网站支付
$ail->unified([
    'method' => 'alipay.trade.wap.pay',

    'out_trade_no' => '',   //商户订单号。由商家自定义，64个字符以内，仅支持字母、数字、下划线且需保证在商户端不重复
    'total_amount' => '',   //单位 元
    'subject' => '会员',    //订单标题

    'notify_url' => 'http://baidu.com',  //异步通知接口
    'return_url' => 'http://baidu.com',  //同步通知接口
]);