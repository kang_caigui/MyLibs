<?php
require_once './vendor/autoload.php';

$config = [
    'appid' => '',
    'appsecret' => '',
    'token' => '',
    'accessToken' => '',
    'encodingAESKey' => '',
    'mch_id' => '',
    'mch_key' => '',
    'sslCertPath' => '',
    'sslKeyPath' => '',
];

$wechat = new \Kangcg\WeChat\WeChat($config);
