<?php
require_once './vendor/autoload.php';

$validator = new \Kangcg\Helper\Validate\Validator();

$data = $validator->validate([
    'appid' => 'sdfsfs',
    'mchid' => 'sdfsfs',
    'description' => 'sdfsfs',
], [
    'appid' => ['required', 'string', 'max' => 32],
    'mchid' => ['required', 'string', 'max' => 32],
    'description' => ['required', 'string', 'max' => 127],
    'out_trade_no' => ['required', 'string', 'max' => 32],
    'time_expire' => ['string', 'max' => 32],
    'attach' => ['string', 'max' => 128],
    'notify_url' => ['required', 'string', 'max' => 255],
    'goods_tag' => ['required', 'string', 'max' => 32],
    'support_fapiao' => ['boolean'],

    'amount' => ['required', 'array', 'rules' => [
        'total' => ['required', 'integer', 'min' => 0],
        'currency' => ['required', 'string', 'max' => 16],
    ]],

    'payer' => ['required', 'array', 'rules' => [
        'openid' => ['required', 'string', 'max' => 128],
    ],
    ],

    'detail' => ['array', 'rules' => [
        'cost_price' => ['required', 'integer', 'min' => 0],
        'invoice_id' => ['required', 'string', 'max' => 32],
    ],
    ],

    'scene_info' => ['array', 'rules' => [
        'payer_client_ip' => ['required', 'string', 'max' => 45],
        'device_id' => ['string', 'max' => 32],
        'store_info' => ['array' => [
            'id' => ['required', 'string', 'max' => 32],
            'name' => ['string', 'max' => 256],
            'area_code' => ['string', 'max' => 32],
            'address' => ['string', 'max' => 512],
        ]],
    ]],

    'settle_info' => ['array', 'rules' => [
        'profit_sharing' => ['boolean']
    ]],

], false);