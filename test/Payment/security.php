<?php
require_once './vendor/autoload.php';

$certificate = \Kangcg\Helper\Security\Certificate::getInstall([
    'key' => 'rggrer',
    'public' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3B4HbZqHP1MIe0VCCkZEmU4ejWanubrA52K0CzAXLrJyoca0jnEd93J1X/eXbGtIMqvM+yToY0M2uN5q60I+fZpRVboReE7mqNM/7QTXNpe/uRgGBebud+cjkD+rgOrvYChEjeFQHzvdivoOClN2EMJfSUbpgWGP8833Txcy9RL/DvzRn+aZ1lwufAE5UR7p+dljdJ3octE6It9Sl2bFv3FxTRS/VpySh2YuOqqA5GiLJJOSbmquKk2i3mT9rsc3tvtIY1t5036cwpdRme5NMD336Lyu6930oVGkXhXLbDNVyixsih8zY3SS3OBWFWCCjUnsEABaZ5bBaCJWPy65AwIDAQAB',
    'private' => 'MIIEpQIBAAKCAQEA3B4HbZqHP1MIe0VCCkZEmU4ejWanubrA52K0CzAXLrJyoca0jnEd93J1X/eXbGtIMqvM+yToY0M2uN5q60I+fZpRVboReE7mqNM/7QTXNpe/uRgGBebud+cjkD+rgOrvYChEjeFQHzvdivoOClN2EMJfSUbpgWGP8833Txcy9RL/DvzRn+aZ1lwufAE5UR7p+dljdJ3octE6It9Sl2bFv3FxTRS/VpySh2YuOqqA5GiLJJOSbmquKk2i3mT9rsc3tvtIY1t5036cwpdRme5NMD336Lyu6930oVGkXhXLbDNVyixsih8zY3SS3OBWFWCCjUnsEABaZ5bBaCJWPy65AwIDAQABAoIBAQDEFSgO1iJ3IbOjhE3RM0l2u+VxlyoAxMGFHiM++CcenS6KuKUtYs0MGnU++SN2HJejM/IVI4q0qjJ8Ov5J9qgOYcLkuC1J3BeT+yOuM6q34jSTYTg0V8TFIS1IGb/gxuhY42Y2calbOYwgeqK5RiIdydjyQO5QKCYBTNBrL0Jgl1TV8nQL8ZzIKAZOB7EY8FkgST6SuJQDleD+fs+lZOTKbaPrkFOY6tuJsbnRN7OiV/8hwY7sV0HNaSQGXBL8hfQhjfjfhCkfIRRJGFFSqzvpTj4Qp+BqYm0NckSNMPnbnNMwPOtKggMYYHh0wdtnJGWBBXSrka1v5aG58Quyi3x5AoGBAPwi1LoMgbaW7fwNBUB5fh1LY0nmZY2ylxBCe9M7ZjrmLrYFeJ8IG9F5o7oTqpcHLySZ26R48q8a7keWg8EqIcccqfZlPM6RHLKK0oknX+3YKAIWE0eearG4GtclW8V6cvKzpmESI4EbxMR5dnzO1KN5yn8J+pl0xsklrKS+/gnHAoGBAN99lV7TYH/gTamqXsPoiOEPZO3Hxb7GsO7OANWrD6Oa2yOaIl81TydM7JdZ1AL9bMBF4UFLxxVWVcKjG+xut5l/1hUrtLvPtbOSRI9KCnFxZS/EPoBny8L9AIq0JOuVZpH59Hca6RV4nKl99pRwpcQsJD3MDkhQn+B6MIaxIDblAoGBAOrR1aTgeP4AWuxfhACkJdxKldIBjZsGNMKj6Ia1z2QTdHV5avlRp/bTfrSSFFJMEo/X60OmkP6b6kcp2tNP1d2GyJP9fUA0zbFKsQGjdighJEGtJY+R7nRNrlGpLWshu2RmBBJrvj1EC2GgbcWXK1qE1x6xwH6iz3AOcleMuTOJAoGBAI30O5s2kdXHiCzbXUP8gGzLlWT+/hh2Jc4ZJSiSdeWKJ+UOMwTcXIFu99wOn36ir5/VAwWxdyuTYfe2GLdtf8rNCxL5ZtvgS95aapfFBKsXxpi3gqeWxjL89oojcEv+4l6YMHIyspgDuyZhLu0MPA5Fp2NlKDFAnLw9gHMdMIQNAoGAWDvij0nf9lTMzX3nF5neaRBdXjcrjtdRsLIJeJ6tjNg3WAJRYCjcIYV4EJwoS/IHYiL1bZ27Z0Sw0kY7wQND0LWSZk+Onm2edvaCTuoCq27Bi9saeB5ZnpWM0AXxFT4VleEyIH3gvBN29VANeAm8tu2TEYaSDdOkQ9aJhl2DxFE=',
]);

//
////加密
$str = $certificate->encode(['sdadad', 'bbbbbbbbbbbbbb']);
echo $str, "\n";
//
////解密
//$str = $certificate->decode($str);
//echo $str, "\n";
//
//$signature = \Kangcg\Helper\Security\Signature::getInstall($certificate);
//
//$pack = $signature->encode(['id' => 5, 'name' => 'kangcg']);
//var_dump($signature->decode($pack));
//
//
//$certificate = new \Kangcg\Helper\Security\Certificate($certificate->getConfig());
//
//$pack = $certificate->encode(['id' => 5, 'name' => 'kangcg']);
//
//var_dump($certificate->decode($pack));

//$op = \Kangcg\Helper\Security\Openssl::getInstall([
//    'key' => 'asdadada',
//    'iv' => 'asdadad'
//]);
//
//var_dump($op->getConfig());
//
//var_dump($op->encode(['sdada', 'fsafsf']));


//加密
$str = $certificate->encode(['sdadad', 'bbbbbbbbbbbbbb']);
echo $str, "\n";

//解密
$str = $certificate->decode($str);
echo $str, "\n";

$signature = \Kangcg\Helper\Security\Signature::getInstall($certificate);

$pack = $signature->encode(['id' => 5, 'name' => 'kangcg']);
var_dump($signature->decode($pack));


$certificate = new \Kangcg\Helper\Security\Certificate($certificate->getConfig());

$pack = $certificate->encode(['id' => 5, 'name' => 'kangcg']);

var_dump($certificate->decode($pack));

$op = \Kangcg\Helper\Security\Openssl::getInstall([
    'key' => 'asdadada',
]);

var_dump($op->getConfig());

var_dump($op->encode('123456'));

var_dump($op->decode("TnBsb1o4LzdkQXNSZnhVWE56czdxdz09"));
