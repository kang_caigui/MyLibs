<?php

require_once './vendor/autoload.php';
header('Content-Type: text/html; charset=utf-8');

$ail = new \Kangcg\Payment\Vendor\WeChat([
    'appid' => '', //公众号ID
    'mchid' => '', //商户号
    'secret' => '', //APIv3密钥
    'serial' => '', //证书序列号
    'private' => '', // apiclient_key.pem的文件内容，可以先将后缀名改为txt，然后获取里面内容
    'notify_url ' => '', //异步通知地址
]);

//JSApi
$ail->unified([
    'method' => 'jsapi',
    'description' => '',       //商品描述
    'out_trade_no' => '',      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    'notify_url' => '',      // 异步接收微信支付结果通知的回调地址
    'amount' => [            // 订单金额信息
        'total'=> 1, //订单总金额，单位为分。
        'currency'=> 'CNY', //选填 货币类型
    ],

    'payer' => [        //支付者信息
        'openid' => '', // 用户在普通商户AppID下的唯一标识。 下单前需获取到用户的OpenID，详见OpenID获取
    ],

    'time_expire' => '',      //选填 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
]);

//APP下单
$ail->unified([
    'method' => 'app',
    'description' => '',       //商品描述
    'out_trade_no' => '',      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    'notify_url' => '',      // 异步接收微信支付结果通知的回调地址
    'amount' => [            // 订单金额信息
        'total'=> 1, //订单总金额，单位为分。
        'currency'=> 'CNY', //选填 货币类型
    ],

    'time_expire' => '',      //选填 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
]);

//H5下单
$ail->unified([
    'method' => 'h5',
    'description' => '',       //商品描述
    'out_trade_no' => '',      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    'notify_url' => '',      // 异步接收微信支付结果通知的回调地址
    'amount' => [            // 订单金额信息
        'total'=> 1, //订单总金额，单位为分。
        'currency'=> 'CNY', //选填 货币类型
    ],

    'scene_info' => [ //场景信息
        'payer_client_ip' => '', //用户终端IP
        'h5_info' => [  // H5场景信息
            'type' => '', //场景类型
        ],
    ],

    'time_expire' => '',      //选填 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE

]);

//二维码支付
$ail->unified([
    'method' => 'native',
    'description' => '',       //商品描述
    'out_trade_no' => '',      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    'notify_url' => '',      // 异步接收微信支付结果通知的回调地址
    'amount' => [            // 订单金额信息
        'total'=> 1, //订单总金额，单位为分。
        'currency'=> 'CNY', //选填 货币类型
    ],

    'time_expire' => '',      //选填 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE

]);

//二维码支付
$ail->unified([
    'method' => 'native',
    'description' => '',       //商品描述
    'out_trade_no' => '',      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
    'notify_url' => '',      // 异步接收微信支付结果通知的回调地址
    'amount' => [            // 订单金额信息
        'total'=> 1, //订单总金额，单位为分。
        'currency'=> 'CNY', //选填 货币类型
    ],

    'time_expire' => '',      //选填 订单失效时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
]);
