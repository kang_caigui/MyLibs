<?php

class a extends  \Kangcg\Helper\Validate\Validator
{
    public function rules() :array
    {
        return [
            'name' => ['date' => ['format' => 'Y/m/d']],
            'id' => ['preg' => '[\d]{1,}$', 'integer'],
        ];
    }

    public function label() :array
    {
        return [];
    }

    public function message() : array
    {
        return [];
    }
}
