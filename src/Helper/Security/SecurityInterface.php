<?php

namespace Kangcg\Helper\Security;

interface SecurityInterface
{
    public function encode($encrypted, bool $isUsePublic = false);

    public function decode(string $encrypted, $isUsePublic = true);

    public function sign(string $str);

    /**
     * 获取配置信息
     * @param null $key
     * @return mixed
     */
    public function getConfig($key = null);

    /**
     * 获取当前前面方式
     * @return string
     */
    public function getEncryption(): string;
}
