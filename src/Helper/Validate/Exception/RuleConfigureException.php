<?php

namespace Kangcg\Helper\Validate\Exception;

class RuleConfigureException extends \Exception
{
    protected $message = "规则配置错误！";
}
