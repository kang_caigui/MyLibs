<?php

namespace Kangcg\Helper\Validate\Exception;

class SafeException extends \Exception
{
    public $message = "安全规则允许为空";
}
