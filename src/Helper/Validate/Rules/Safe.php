<?php

namespace Kangcg\Helper\Validate\Rules;


use Kangcg\Helper\Validate\Exception\SafeException;
use Kangcg\Helper\Validate\Validator;

class Safe implements RuleInterface
{
    use RuleTrait;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        throw new SafeException();
    }
}
