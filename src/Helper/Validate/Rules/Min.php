<?php

namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Validator;

class Min implements RuleInterface
{
    use RuleTrait;

    public $min;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if ($value < $this->min){
            return $validator->setError($this->getMessage($field, $this->min));
        }
    }

    public function extra($extra)
    {
        if(isset($extra['min'])){
            $this->min = $extra['min'];
            return $extra;
        }else if(is_int($extra)){
            return [
                'min' => $extra
            ];
        }

        throw new RuleConfigureException("验证规则 Min 配置错误，期望【 min => 15 】!");
    }
}
