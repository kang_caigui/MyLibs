<?php

namespace Kangcg\Helper\Validate\Rules;


use Kangcg\Helper\Validate\Validator;

class Arr implements RuleInterface
{
    use RuleTrait;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if(!is_array($value)){
            return $validator->setError($this->getMessage($field));
        }
    }
}
