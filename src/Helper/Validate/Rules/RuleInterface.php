<?php
namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Validator;

interface RuleInterface
{
    /**
     *
     * @param $value
     * @param string $field
     * @param array $input
     * @param Validator $validator
     * @return mixed
     */
    public function run(&$value, string $field, array $input, Validator $validator);

    public function setRules($rules);

    public function isRule($rule) : bool;
    /**
     * 规则而外的信息
     * @param array|string|integer|bool $config
     * @return mixed
     */
    public function setConfig($config);

    public function extra($extra);
}
