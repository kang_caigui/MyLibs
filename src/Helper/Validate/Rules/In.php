<?php

namespace Kangcg\Helper\Validate\Rules;



use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Validator;

class In implements RuleInterface
{
    use RuleTrait;

    public $in;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if(!in_array($value, $this->in)){
            return $validator->setError($this->getMessage($field, implode(',', $this->in)));
        }
    }

    public function extra($extra){
        if(!is_array($extra)){
            throw new RuleConfigureException("in 规则配置错误，配置规则为 in => [1,2,3]");
        }

        if(isset($extra['in'])){
           return $extra;
        }

        return [
            'in' => $extra
        ];
    }
}
