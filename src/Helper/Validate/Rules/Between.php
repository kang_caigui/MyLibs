<?php

namespace Kangcg\Helper\Validate\Rules;


use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Validator;

class Between implements RuleInterface
{
    use RuleTrait;

    public $between = [];
    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if(!($value >= $this->between[0] && $value <= $this->between[1])){
            return $validator->setError($this->getMessage($field, implode(', ', $this->between[1])));
        }
    }

    public function extra($extra)
    {
        if(is_array($extra)){
            if(isset($extra['between'])){
                return $extra;
            }

            if(isset($extra[0]) && is_array($extra[1])){
                return [
                    'between' => $extra
                ];
            }
        }

        throw new RuleConfigureException("验证规则 Between 配置错误，期望【 between => [0, 15] | between => [ between => [0, 15]] 】!");
    }
}
