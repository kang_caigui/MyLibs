<?php

namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Validator;

class Max implements RuleInterface
{
    use RuleTrait;

    public $max;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if ($value > $this->max){
            return $validator->setError($this->getMessage($field, $this->max, ":attribute 不能大于【:message】！"));
        }
    }

    public function extra($extra)
    {
        if(isset($extra['max'])){
            return $extra;
        }else if(is_int($extra)){
           return [
               'max' => $extra
           ];
        }

        throw new RuleConfigureException("验证规则 Max 配置错误，期望【 max => 15 】!");
    }
}
