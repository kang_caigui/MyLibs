<?php

namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Validator;

class NotIn implements RuleInterface
{
    use RuleTrait;

    public $in = [];

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if (in_array($value, $this->in)) {
            return $validator->setError($this->getMessage($field, implode(',', $this->in)));
        }
    }

    public function extra($extra)
    {
        if (!is_array($extra)) {
            throw new RuleConfigureException("not in 规则配置错误，配置规则为 notin => [1,2,3]");
        }

        if (isset($extra['in'])) {
            if (!is_array($extra['in'])) {
                throw new RuleConfigureException("not in 规则配置错误，配置规则为 notin => [1,2,3]");
            }

            return $extra;
        }

        return [
            'in' => $extra
        ];
    }
}
