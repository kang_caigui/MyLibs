<?php

namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Base;
use Kangcg\Helper\Validate\Exception\ValidateException;
use Kangcg\Helper\Validate\Validator;

class Required implements RuleInterface
{
    use RuleTrait;

    protected function beforeRun(&$value) : bool{
        return true;
    }

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if($value === null || $value === ''){
            $validator->setError($this->getMessage($field));
            return false;
        }
    }
}
