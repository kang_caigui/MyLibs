<?php

namespace Kangcg\Helper\Validate\Rules;


use Kangcg\Helper\Validate\Validator;

class Date implements RuleInterface
{
    use RuleTrait;

    public $format = "Y-m-d H:i:s";

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        $dateTime = \DateTime::createFromFormat($this->format, $value);
        if($dateTime === false || array_sum($dateTime->getLastErrors()) > 0){
            return $validator->setError($this->getMessage($field, $this->format));
        }
    }

    public function extra($extra)
    {
        if(isset($extra['format'])){
            return $extra;
        }

        if(is_string($extra)){
            return [
                'format' => $extra,
            ];
        }

        return $extra;
    }
}
