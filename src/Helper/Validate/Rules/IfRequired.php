<?php

namespace Kangcg\Helper\Validate\Rules;

use Kangcg\Helper\Validate\Base;
use Kangcg\Helper\Validate\Exception\RuleConfigureException;
use Kangcg\Helper\Validate\Exception\ValidateException;
use Kangcg\Helper\Validate\Validator;

class IfRequired implements RuleInterface
{
    use RuleTrait;

    public $field;

    protected function beforeRun(&$value) : bool{
        return true;
    }

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        if($value === null || $value === ''){
            $validator->setError($this->getMessage($field));
            return false;
        }
    }

    public function extra($extra)
    {
        if(isset($extra['field'])){
           return $extra;
        }

        if(is_string($extra)){
            return [
                'field' => $extra,
            ];
        }

        throw new RuleConfigureException("验证规则 IfRequired 配置错误，期望【 if_required => '数据字段名' | if_required => [ if_required => 数据字段名'] 】!");
    }
}
