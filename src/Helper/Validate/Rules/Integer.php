<?php

namespace Kangcg\Helper\Validate\Rules;



use Kangcg\Helper\Validate\Validator;

class Integer implements RuleInterface
{
    use RuleTrait;
    public $max = null;

    public $min = null;

    protected function afterRun(&$value, string $field, array $input, Validator $validator)
    {
        $pattern = "/^\d+$/";
        if(!preg_match($pattern, $value)){
            return $validator->setError($this->getMessage($field));
        }

        $value = intval($value);
        if($this->max !== null){
            $bool = $validator->getRuleInstall('max', $this->_rules, ['max' => $this->max])->run($value, $field, $input, $validator);
            if($bool === false){
                return false;
            }
        }

        if($this->min !== null){
            $bool = $validator->getRuleInstall('min', $this->_rules, ['min' => $this->max])->run($value, $field, $input, $validator);
            if($bool === false){
                return false;
            }
        }
    }

    public function extra($extra)
    {
        if(isset($extra['max']) || isset($extra['min'])){
            return $extra;
        }

        if(is_int($extra)){
            return [
                'max' => $extra,
            ];
        }

        return $extra;
    }
}
