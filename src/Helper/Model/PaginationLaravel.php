<?php

declare(strict_types=1);

namespace Kangcg\Helper\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait PaginationLaravel
{
    /**
     * 关键字保留 OR
     * @var array
     * [
     * '数据库字段',
     * '表单字段' => '数据库字段',
     * ['数据库字段', ],
     * ['数据库字段', '查询条件', ],
     * ['数据库字段', '查询条件', '替换'],
     * LIKE 示例 =>['数据库字段', 'LIKE', '--数据库字段--%'],
     * 'or' =>
     * 'or' => ['数据库字段', ],
     * 'or' => ['数据库字段', '查询条件',],
     * 'or' => ['数据库字段', '查询条件', '替换'],
     * ['or' => '',
     * '数据库字段',
     * ['数据库字段', ],
     * ['数据库字段', '查询条件', ],
     * ['数据库字段', '查询条件', '替换'],
     * ...
     * ],
     * ]
     */
    protected array $search = [];
    protected array|string|null $with = null;
    protected array|string $columns = ['*'];
    protected int $defaultLimit = 15;
    protected array $defaultSort = ['id' => 'DESC'];
    protected array $allowSort = [];
    protected string $pageName = 'page';
    protected string $limitName = 'limit';
    public abstract function getModel(): Model;

    public function search(array $input = [])
    {
        $query = $this->parseQuery($this->search, $input, $this->getModel()->newQuery());
        $query = $this->parseOrder($query, $input, $this->defaultSort, $this->allowSort);
        $paginate = $query->paginate($this->getLimit($input), $this->columns, $this->pageName, $this->getPage($input));

        $data['total'] = $paginate->total();
        $data['limit'] = $paginate->perPage();
        $data['page'] = $paginate->currentPage();
        $data['list'] = $paginate->items();
        $this->responseBefore($data, $paginate);

        return $data;
    }

    protected function responseBefore(array &$data, $paginate): void
    {

    }

    private function parseOrder(Builder|Model $query, array $input, array $defaultSort, array $allowSort = []): Builder|Model
    {
        if (!empty($input['sort']) && $sorts = trim($input['sort'])) {
            $sorts = explode(',', $sorts);
            foreach ($sorts as $sort) {
                if (!isset($allowSort[$sort[0]])) {
                    continue;
                }

                $defaultSort[$sort[0]] = isset($sort[1]) ? ($sort[1] == 'ASC' ? 'ASC' : 'DESC') : 'DESC';
            }
        }

        foreach ($defaultSort as $field => $sort) {
            $query = $query->orderBy($field, $sort);
        }

        return $query;
    }

    private function parseQuery(array $search, array $input, Builder|Model $query): Builder|Model
    {
        foreach ($search as $key => $field) {
            if ($this->isClosure($field)) {
                $query = $this->isString($key) ? $query->where($key, $field)
                    : $field($query, $input);

                if (!($query instanceof Builder || $query instanceof Model)) {
                    throw new \Exception(var_export($query . "返回未继承 Builder | Model", true));
                }
            } else if ($this->isInt($key)) {
                $query = $this->parseQueryAnd($query, $field, $input);
            } else if ($key === 'OR') {
                $query = $query->orWhere(function ($query) use ($input, $field) {
                    return $this->parseQuery($field, $input, $query);
                });
            } else if ($this->isString($key) && $this->isInputFieldNotEmpty($input, $key)) {
                $value = $input[$key] ?? '';
                $key = is_array($field) ? $field[0] : $field;
                $input[$key] = $value;

                $query = $this->parseQueryAnd($query, $field, $input);
            }
        }

        return $query;
    }

    private function parseQueryAnd(Builder|Model $query, $field, $input): Builder|Model
    {
        if (is_string($field) && isset($input[$field]) && $this->isInputFieldNotEmpty($input, $field)) {
            $query = $query->where($field, '=', $input[$field]);
        } elseif ($this->isOr($field)) {
            $query = $query->where(function ($query) use ($field, $input) {
                return $this->parseQuery($field, $input, $query);
            });
        } else if (is_array($field) && $this->isEmpty($field, $input)) {
            if (method_exists($query, $field[1])) {
                $query = $query->{$field[1]}($field[0], $field[2]);
            } else {
                $query = $query->where($field[0], $field[1], $field[2]);
            }
        }

        return $query;
    }

    /**
     * @param $search
     * @return bool
     */
    private function isOr($search)
    {
        return is_array($search) && isset($search['OR']);
    }

    private function isValueEmpty($value)
    {
        return $value === '' || $value === null;
    }

    /**
     * @param array $input
     * @param string $field
     * @return bool
     */
    private function isInputFieldNotEmpty(array $input, string $field): bool
    {
        if (!isset($input[$field])) {
            return false;
        }

        return !$this->isValueEmpty($input[$field]);
    }

    private function isEmpty(&$value, $input)
    {
        if (isset($value[2]) && $this->isClosure($value[2])) {
            $value[2] = $value[2]($input);
            return !empty($value[2]);
        }

        if (count($value) === 3 && strrpos(strval($value[2]), "--{$value[0]}--") === false) {
            return true;
        }

        if (!isset($input[$value[0]]) || $this->isValueEmpty($input[$value[0]])) {
            return false;
        }

        $value[2] = isset($value[2]) ? str_replace('--' . $value[0] . '--', $input[$value[0]], $value[2]) : $input[$value[0]];
        return true;
    }

    public function getLimit(array $input): int
    {
        $limit = $input[$this->limitName] ?? $this->defaultLimit;
        return $limit > 0 ? $limit : $this->defaultLimit;
    }

    public function getPage(array $input): int
    {
        $page = $input[$this->pageName] ?? 1;
        return $page > 0 ? $page : 1;
    }


    /**
     * @param $value
     * @return bool
     */
    private function isClosure($value): bool
    {
        return $value instanceof \Closure;
    }

    /**
     * @param $value
     * @return bool
     */
    private function isInt($value): bool
    {
        return is_int($value);
    }

    /**
     * @param $value
     * @return bool
     */
    private function isString($value): bool
    {
        return is_string($value);
    }

}
