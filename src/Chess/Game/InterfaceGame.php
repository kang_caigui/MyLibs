<?php

namespace Kangcg\Chess\Game;

interface InterfaceGame
{
    /**
     * 获取发牌结果
     * @return array
     */
    public function licensing(): array;
}
