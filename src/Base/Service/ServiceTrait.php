<?php

namespace Kangcg\Base\Service;

use Kangcg\Base\Component;

abstract class ServiceTrait extends Component
{
    const LIMIT = 15;
    const LIMIT_MAX = 100;
    const LIMIT_MIN = 5;

    const EVENT_SEARCH = 'search';

    const WITH_ACTION = 'with';
    const WHERE_AND_ACTION = 'where';
    const WHERE_OR_ACTION = 'orWhere';
    const ORDER_BY_ACTION = 'orderBy';

    abstract function getQuery();
    abstract public function search(array $search = [], $with = null, array $columns = ['*'], array $defaultSort = ['id' => 'DESC'], $allowSort = []);
    protected function parseQuery(array $search, array $input, $query, $with = null, array $defaultSort = [], array $allowSort = [])
    {
        $or = isset($search['or']) ? $search['or'] : [];
        unset($search['or']);

        if (!empty($or)) {
            $query = $query->{static::WHERE_AND_ACTION}($query, function () use ($search, $input, $query) {
                return $this->parseArray($query, $search, $input);
            });

            $query = $query->{static::WHERE_OR_ACTION}($query, function () use ($or, $input, $query) {
                return $this->parseArray($query, $or, $input);
            });
        } else {
            $query = $this->parseArray($query, $search, $input);
        }

        $query = $this->parseOrder($query, $input, $defaultSort, $allowSort);

        return $with ? $query->{static::WITH_ACTION}($with) : $query;
    }

    protected function parseOrder($query, $input, array $defaultSort = [], array $allowSort = [])
    {
        if (isset($input['sort'])) {
            $sorts = explode(',', trim($input['sort']));
            foreach ($sorts as $sort) {
                $sort = explode('=', trim($sort));
                if (!isset($allowSort[$sort[0]])) {
                    continue;
                }

                $defaultSort[$sort[0]] = isset($sort[1]) ? ($sort[1] == 'ASC' ? 'ASC' : 'DESC') : 'DESC';
            }
        }

        foreach ($defaultSort as $field => $sort) {
            $query = $query->{static::ORDER_BY_ACTION}($field, $sort);
        }

        return $query;
    }

    protected function parseArray($query, array $search, array $input, $isDebug = false)
    {
        $or = isset($search['or']) ? $search['or'] : [];
        unset($search['or']);
        if (!empty($or)) {
            $query = $query->{static::WHERE_AND_ACTION}($query, function () use ($search, $input, $query) {
                return $this->parseArray($query, $search, $input);
            });

            $query = $query->{static::WHERE_OR_ACTION}($query, function () use ($or, $input, $query) {
                return $this->parseArray($query, $or, $input);
            });

            return $query;
        }

        foreach ($search as $key => $field) {
            if ($field instanceof \Closure) {
                $query->{static::WHERE_AND_ACTION}($query, $field, $input);
            } elseif (is_int($key)) {
                $query = $this->parseQueryCondition($query, $field, $input);
            } else {
                $query = $this->parseQueryCondition($query, $field, $input, $key);
            }
        }
    }

    protected function parseQueryCondition($query, $field, $input, $mapInput = null, $operate = ServiceTrait::WHERE_AND_ACTION)
    {
        if (is_string($field) && isset($input[$field]) && $this->isValueEmpty($input[$field]) === false) {
            $query = $query->$operate($query, $field, '=', $input[$field]);
        } elseif (is_array($field) && isset($field['or'])) {
            $query = $this->parseArray($query, $field, $input, true);
        } elseif (is_array($field) && $this->isEmpty($field, $input, $mapInput)) {
            $query = $query->$operate($query, $field[0], $field[2], $field[1]);
        }

        return $query;
    }

    private function isEmpty(&$value, $input, $mapInput = null)
    {
        if (isset($value[2]) && $value[2] instanceof \Closure) {
            return $value[2] = $value[2]($input);
        }

        if (isset($value[2]) && strrpos($value[2], "--{$value[0]}--") === false) {
            return true;
        }

        $inputKey = $mapInput === null ? (isset($input[$value[0]]) ? $input[$value[0]] : null) : $mapInput;
        $inputValue = isset($input[$inputKey]) ? $input[$inputKey] : null;
        if ($this->isValueEmpty($inputValue)) {
            return false;
        }

        $value[2] = isset($value[2]) ? str_replace('--' . $value[0] . '--', $input[$value[0]], $inputValue) : $inputValue;
        return true;
    }

    protected function isValueEmpty($value): bool
    {
        return false;
        return $value === null || $value === '';
    }
}


