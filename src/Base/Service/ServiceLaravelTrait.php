<?php

namespace Kangcg\Base\Service;

use Kangcg\Base\Component;
use Kangcg\Base\Event;

abstract class ServiceLaravelTrait extends ServiceTrait
{
    abstract function getQuery();

    public function search(array $search = [], $with = null, array $columns = ['*'], array $defaultSort = ['id' => 'DESC'], $allowSort = [])
    {
        $input = request()->all();
        $limit = isset($input['limit']) ? intval($input['limit']) : self::LIMIT;
        $limit = $limit >= self::LIMIT_MAX ? self::LIMIT_MAX : ($limit < self::LIMIT_MIN ? self::LIMIT_MIN : $limit);

        $paginate = $this->parseQuery($search, $input, $this->getQuery())
            ->paginate($limit, $columns);

        $data['current_page'] = $paginate->currentPage();
        $data['last_page'] = $paginate->lastPage();
        $data['total'] = $paginate->total();
        $data['limit'] = $limit;
        $data['list'] = $paginate->items();

        $event = new Event();
        $event->data = $data;
        $this->trigger(self::EVENT_SEARCH, $event);

        return $event->data;
    }
}


