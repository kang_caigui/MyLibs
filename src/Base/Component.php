<?php

namespace Kangcg\Base;

use ArrayAccess;
use Closure;

class Component implements ArrayAccess
{
    public static $app;

    public  $container = [];

    public  $alias = [];

    private static $_install = null;
    public function __construct($config = [])
    {
        $this->setConfig($this, $config);
        $this->init();
    }

    /**
     * 获取该类扩展为该类提供服务
     * @return array
     */
    public function behaviors(): array
    {
        return [];
    }

    public  function getInstall($configure = [])
    {
        if(is_string($configure)){
            $config['class'] = $configure;
        }else{
            $config = $configure;
        }

        $class = $config['class'];
        unset($config['class']);
        $class = new $class($config);
        $this->setConfig($class, $config);
        return $class;
    }
    /**
     * 伪多继承
     * @return array   [ '扩展名称' => 扩展实例或者其他]
     * [
     * 'redis' => new \Redis,
     * 'redis' => \Redis::class,
     * 'redis' => [
     * 'class' => \Redis::class
     * 127.0.0.1 //其他参数
     * 6399 //其他参数
     * ],
     * 'redis' => [
     * \Redis::class,
     * 127.0.0.1 //其他参数
     * 6399 //其他参数
     * ],
     * ]
     *
     *
     */
    public function extends()
    {
        return [];
    }

    /**
     * 事件触发
     * @param string $name 事件名称
     * @param Event|null $event
     */
    public function trigger(string $name, Event $event = null)
    {
        $this->ensureBehaviors();
        if (!empty($this->_events[$name])) {
            $eventHandlers = $this->_events[$name];
            if ($event === null) {
                $event = new Event();
                $event->handled = false;
            }

            $event->name = $name;
            $event->sender = $this;

            foreach ($eventHandlers as $handler) {
                $event->onData = $handler[1];
                call_user_func($handler[0], $event);
                if ($event->handled) {
                    return;
                }
            }
        }
    }

    /**
     * 获取指定扩展
     * @param $name
     */
    public function getBehavior($name)
    {
        $this->ensureBehaviors();
        if (!isset($this->_behaviors[$name])) {
            throw new ErrorBase($name . ' extension not exist');
        }

        return $this->_behaviors[$name];
    }

    /**
     * @param $name
     * @param $behavior
     * @return $this
     */
    public function addBehavior($name, $behavior)
    {
        $this->ensureBehaviors();
        $this->attachBehaviorInternal($name, $behavior);
        return $this;
    }

    /**
     * 添加事件监听
     * @param string $name 事件名称
     * @param array | Closure $handler
     * @param null $data
     * @param bool $append
     * @return $this
     * @throws ErrorBase
     */
    public function on($name, $handler, $data = null, $append = true)
    {
        $this->ensureBehaviors();
        if ($append == false) {
            unset($this->_events[$name]);
        }

        $this->_events[$name][] = [$handler, $data];
        return $this;
    }

    /**
     * 解除事件
     * @param string $name 事件名称
     * @return $this
     * @throws ErrorBase
     */
    public function off($name)
    {
        $this->ensureBehaviors();
        unset($this->_events[$name]);
        return $this;
    }

    /**
     * @throws ErrorBase
     */
    public function ensureBehaviors()
    {
        if ($this->_behaviors === null) {
            $this->_behaviors = [];
            foreach ($this->behaviors() as $name => $behavior) {
                $this->attachBehaviorInternal($name, $behavior);
            }
        }
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getExtend($name)
    {
        $this->ensureExtends();
        return $this->_extends[$name] ?? null;
    }

    public function ensureExtends()
    {
        if ($this->_extends === null) {
            $this->_extends = [];
            foreach ($this->extends() as $name => $extend) {
                $this->attachExtendInternal($name, $extend);
            }
        }
    }

    private function attachExtendInternal($name, $extend)
    {
        try {
            if (!is_object($extend)) {
                $class = '';
                if (is_string($extend)) {
                    $class = $extend;
                } else if (is_array($extend)) {
                    if (isset($extend['class'])) {
                        $class = $extend['class'];
                        unset($extend['class']);
                        $params = $extend;
                    } else {
                        $class = array_shift($extend);
                        $params = $extend;
                    }
                }

                $params['sender'] = $this;
                $extend = new $class($params);
            }
        } catch (\Exception $exception) {
            throw new ErrorBase($name . '扩展' . $exception->getMessage(), 1, $exception);
        }

        $this->_extends[$name] = $extend;
    }

    /**
     * @param $name
     * @param $behavior
     * @return Behavior|mixed
     * @throws ErrorBase
     */
    private function attachBehaviorInternal($name, $behavior)
    {
        if (empty($behavior)) {
            return null;
        }

        try {
            if (!is_object($behavior)) {
                $behavior = new $behavior;
            }

            if ($behavior instanceof Behavior) {
                $behavior->attach($this);
            }
        } catch (\Exception $exception) {
            throw new ErrorBase($exception->getMessage(), 1, $exception);
        }

        if (is_int($name)) {
            $this->_behaviors[] = $behavior;
        } else {
            $this->_behaviors[$name] = $behavior;
        }

        return $behavior;
    }

    /**
     * 设置初始配置信息!
     * @param array $config
     */
    public function setConfig($obj, $config = [])
    {
        foreach ($config as $attr => $value) {
            $obj->{$attr} = $value;
        }
    }

    /**
     * 获取配置信息
     * @return array
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $attr = 'set' . ucfirst($name);
        if (method_exists($this, $attr)) {
            $this->{$attr}($value);
        } else {
            $this->_config[$name] = $value;
        }

        return $this;
    }

    /**
     * @param $name
     * @return |null
     */
    public function __get($name)
    {
        $attr = 'get' . ucfirst($name);
        if (method_exists($this, $attr)) {
            return $this->{$attr}();
        }

        return $this->_config[$name] ?? $this->getExtend($name);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws ExceptionError
     */
    public function __call($name, $arguments)
    {
        $arguments[] = $this;
        $this->ensureExtends();
        foreach ($this->_extends as $extends) {
            if (method_exists($extends, $name)) {
                return call_user_func_array([$extends, $name], $arguments);
            }
        }

        throw new ErrorBase('当前类方法不存在:' . $name);
    }

    /**
     * 设置错误信息
     * @param string $errorCode 错误码
     * @param string $errorMsg 错误信息
     * @return bool
     */
    public function setErrors($errorCode, $errorMsg)
    {
        $this->_errors[] = [
            'errcode' => $errorCode,
            'errmsg' => $errorMsg,
        ];

        return false;
    }

    public function setError($errorMsg)
    {
        return $this->setErrors(-200, $errorMsg);
    }
    /**
     * 获取错误码和错误信息
     * @return array ['errcode' => '', 'errmsg' => '']
     */
    public function getErrors()
    {
        return  $this->_errors;
    }

    /**
     * @param string $key errmsg | errcode
     * @return false|mixed|string
     */
    public function getError($key = 'errmsg')
    {
        $error = current($this->_errors);
        return isset($error[$key]) ? $error[$key] : $error;
    }
    /**
     * 获取错误响应码
     * @return string
     */
    public function getErrorCode()
    {
        return $this->getError('errcode');
    }

    /**
     * 获取错误信息
     * @return string
     */
    public function getErrorMsg()
    {
        return $this->getError();
    }

    public function init()
    {
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_config[] = $value;
        } else {
            $this->_config[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->_config[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->_config[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_config[$offset] ?? null;
    }

    protected $_config = [];
    private $_events = [];
    private $_errors = [];

    private $_behaviors = null;
    private $_extends = null;
}
