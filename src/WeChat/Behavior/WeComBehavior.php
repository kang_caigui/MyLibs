<?php

namespace Kangcg\WeChat\Behavior;

use Kangcg\Base\Behavior;
use Kangcg\Base\Event;
use Kangcg\Helper\Cache\FileCache;
use Kangcg\WeChat\WeCom;

class WeComBehavior extends Behavior
{
    public function events()
    {
        return [
            WeCom::EVENT_BEFORE_REFRESH_ACCESS_TOKEN => [$this, 'beforeRefreshAccessToken'],
            WeCom::EVENT_AFTER_REFRESH_ACCESS_TOKEN => [$this, 'afterRefreshAccessToken'],
        ];
    }

    /**
     * 监听AccessToken42001错误
     * @param Event $event
     */
    public function errorAccessToken(Event $event)
    {
        FileCache::getInstall()->set($event->sender->getAccessTokenCacheKey(), null);
    }

    /**
     * 监听AccessToken更新之前事件
     * @param Event $event
     */
    public function beforeRefreshAccessToken(Event $event)
    {
        if ($event->data['access_token'] = FileCache::getInstall()->get($event->sender->getAccessTokenCacheKey())) {
            $event->handled = true;
        }
    }

    /**
     * 监听AccessToken更新之后事件
     * @param Event $event
     */
    public function afterRefreshAccessToken(Event $event)
    {
        if (!empty($event->data['access_token'])) {
            FileCache::getInstall($this->getLogsPath())->set($event->sender->getAccessTokenCacheKey(), $event->data['access_token'], 7150);
        }
    }

    /**
     * 日志事件
     * @param Event $event
     */
    public function log(Event $event)
    {

    }

    private function getLogsPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;
    }
}
