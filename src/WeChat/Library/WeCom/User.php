<?php

namespace Kangcg\WeChat\Library\WeCom;

use Kangcg\WeChat\Library\Urls;

trait User
{
    /**
     * @param array $data =
     * [
     * 'userid' => 'zhangsan', //成员UserID。对应管理端的帐号，企业内必须唯一。长度为1~64个字节。只能由数字、字母和“_-@.”四种字符组成，且第一个字符必须是数字或字母。系统进行唯一性检查时会忽略大小写。
     * 'name' => '张三', //成员名称。长度为1~64个utf8字符
     * 'department' => [1, 2], //成员所属部门id列表，不超过100个
     * 'mobile' => '+86 13800000000', //手机号码。企业内必须唯一，mobile/email二者不能同时为空
     * 'email' => 'zhangsan@gzdev . com', //邮箱。长度6~64个字节，且为有效的email格式。企业内必须唯一，mobile/email二者不能同时为空
     * 'alias' => 'jackzhang', //成员别名。长度1~64个utf8字符
     * 'order' => 0, //部门内的排序值，数值越大排序越前面。有效的值范围是[0, 2^32)
     * 'position' => '产品经理', //职务信息。长度为0~128个字符
     * 'gender' => '1', //性别。1表示男性，2表示女性
     * 'biz_mail' => 'zhangsan@qyycs2 . wecom . work', //企业邮箱。仅对开通企业邮箱的企业有效。长度6~64个字节，且为有效的企业邮箱格式。企业内必须唯一。未填写则系统会为用户生成默认企业邮箱（由系统生成的邮箱可修改一次，2022年4月25日之后创建的成员需通过企业管理后台-协作-邮件-邮箱管理-成员邮箱修改）
     * 'is_leader_in_dept' => [1, 0], //个数必须和参数department的个数一致，表示在所在的部门内是否为部门负责人。1表示为部门负责人，0表示非部门负责人。在审批(自建、第三方)等应用里可以用来标识上级审批人
     * 'direct_leader' => ['lisi', 'wangwu'], //直属上级UserID，设置范围为企业内成员，可以设置最多5个上级
     * 'enable' => 1, //启用/禁用成员。1表示启用成员，0表示禁用成员
     * 'avatar_mediaid' => '2 - G6nrLmr5EC3MNb_ - zL1dDdzkd0p7cNliYu9V5w7o8K0', //成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
     * 'telephone' => '020 - 123456', //座机。32字节以内，由纯数字、“-”、“+”或“,”组成。
     * 'address' => '广州市海珠区新港中路', //地址。长度最大128个字符
     * 'main_department' => 1, //主部门
     * 'extattr' => [], //自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。
     * ]
     * @return bool
     */
    public function userCreate($data)
    {
        if (!$result = $this->httpPost(Urls::COMPANY_USER_CREATE_URL, $data, true)) {
            return false;
        }

        return true;
    }

    /**
     * 企业可通过接口批量邀请成员使用企业微信，邀请后将通过短信或邮件下发通知。
     * @param array $user 成员ID列表, 最多支持1000个
     * @param array $party 部门ID列表，最多支持100个。
     * @param array $tag 标签ID列表，最多支持100个。
     * @return bool
     */
    public function userInviteBatch($user = [], $party = [], $tag = [])
    {
        $data['user'] = $user;
        $data['party'] = $party;
        $data['tag'] = $tag;
        if ($this->httpPost(Urls::COMPANY_USER_BATCH_INVITE_URL, $data, true)) {
            return true;
        }

        return false;
    }

    /**
     * 更新成员
     * @param array $data 查看添加
     * @return bool
     */
    public function userUpdate($data)
    {
        if (!$result = $this->httpPost(Urls::COMPANY_USER_UPDATE_URL, $data, true)) {
            return false;
        }

        return true;
    }

    /**
     * 获取成员信息
     * @param string $userid
     * @return bool|array
     */
    public function userGet($userid)
    {
        $data['userid'] = $userid;
        return $this->httpGet(Urls::COMPANY_USER_FIND_URL, $data, true);
    }

    /**
     * 删除成员信息
     * @param string $userid
     * @return bool
     */
    public function userDel($userid)
    {
        $data['userid'] = $userid;
        if ($this->httpGet(Urls::COMPANY_USER_DEL_URL, $data, true)) {
            return true;
        }

        return false;
    }

    /**
     * 批量删除成员信息
     * @param array $userIds ['sdfs', 'dadad']
     * @return bool
     */
    public function userBatchDel($userIds)
    {
        $data['useridlist'] = $userIds;
        if ($this->httpPost(Urls::COMPANY_USER_BATCH_DEL_URL, $data, true)) {
            return true;
        }

        return false;
    }

    /**
     * 获取部门成员
     * @param string $department_id
     * @return bool|array ['userid', 'name', 'department', 'open_userid']
     */
    public function userDepartmentSelect($department_id)
    {
        $data['department_id'] = $department_id;
        if ($result = $this->httpGet(Urls::COMPANY_USER_DEP_LIST_URL, $data, true)) {
            return $result['userlist'];
        }

        return false;
    }

    /**
     * 获取部门成员明细
     * @param string $department_id
     * @return bool|mixed
     */
    public function userInfoDepartmentSelect($department_id)
    {
        $data['department_id'] = $department_id;
        if ($result = $this->httpGet(Urls::COMPANY_USER_LIST_URL, $data, true)) {
            return $result['userlist'];
        }

        return false;
    }

    /**
     * userId 转换为 openid
     * @param string $userid
     * @return bool|mixed
     */
    public function userIdToOpenid($userid)
    {
        $data['userid'] = $userid;
        if ($result = $this->httpPost(Urls::COMPANY_USER_ID_TO_OPENID_URL, $data, true)) {
            return $result['openid'];
        }

        return false;
    }

    /**
     * openid 转换为 userId
     * @param $openid
     * @return bool|mixed
     */
    public function openIdToUserId($openid)
    {
        $data['openid'] = $openid;
        if ($result = $this->httpPost(Urls::COMPANY_OPENID_TO_USER_ID_URL, $data, true)) {
            return $result['userid'];
        }

        return false;
    }

    /**
     * 用户二次验证 如果成员是首次加入企业，企业获取到userid，并验证了成员信息后，调用如下接口即可让成员成功加入企业
     * @param $userid
     * @return bool
     */
    public function userAuth($userid)
    {
        $data['userid'] = $userid;
        if ($this->httpGet(Urls::COMPANY_USER_AUTH_URL, $data, true)) {
            return true;
        }

        return false;
    }

    /**
     * 获取加入企业二维码
     * @param string $size_type 1: 171 x 171   2: 399 x 399   3: 741 x 741   4: 2052 x 2052
     * @return bool|mixed
     */
    public function userInviteQrcode($size_type = 2)
    {
        $url = Urls::COMPANY_USER_QRCODE_INVITE_URL . '&size_type' . $size_type;
        if ($result = $this->httpGet($url, null, true)) {
            return $result['join_qrcode'];
        }

        return false;
    }

    /**
     * @vasr 手机号获取userid
     * @param string $mobile
     * @return bool|mixed
     */
    public function userIdMobile($mobile)
    {
        $data['mobile'] = $mobile;
        if ($result = $this->httpPost(Urls::COMPANY_USER_ID_GET_URL, $data, true)) {
            return $result['userid'];
        }

        return false;
    }

    /**
     * 邮箱获取userid
     * @param string $email
     * @param null $email_type 邮箱类型：1-企业邮箱（默认）；2-个人邮箱
     * @return bool|mixed
     */
    public function userIdEmail($email, $email_type = null)
    {
        $data['email'] = $email;
        $data['email_type'] = $email_type;
        if ($result = $this->httpPost(Urls::COMPANY_USER_ID_GET_EMAIL_URL, $data, true)) {
            return $result['userid'];
        }

        return false;
    }

    /**
     * 获取成员ID列表
     * @param null $cursor 用于分页查询的游标，字符串类型，由上一次调用返回，首次调用不填
     * @param int $limit 分页，预期请求的数据量，取值范围 1 ~ 10000
     * @return bool| ['next_cursor' => '', 'dept_user' => [ [userid, 'department']]]
     */
    public function userIdsSelect($cursor = null, $limit = 10000)
    {
        $data['cursor'] = $cursor;
        $data['limit'] = $limit;
        if ($result = $this->httpPost(Urls::COMPANY_USER_ID_SELECT_URL, $data, true)) {
            return $result;
        }

        return false;
    }
}
