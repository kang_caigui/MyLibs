<?php

namespace Kangcg\WeChat\Library\Component;

use Kangcg\WeChat\Library\Urls;

/**
 * 公众号-小程序
 * Trait Appid
 * @package Kang\Libs\WeChat\Library
 */
trait Appid
{
    /**
     * 获取预授权码
     * @return bool|string pre_auth_code
     */
    public function appidPreAuthCodeGet()
    {
        $data['component_appid'] = $this->component_appid;
        if (!$result = $this->httpPost(Urls::PRE_AUTH_CODE_URL, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result['pre_auth_code'];
    }

    /**
     * 获取预授权地址
     * @param string $redirectUrl 管理员授权确认之后会自动跳转进入回调 URI，并在 URL 参数中返回授权码和过期时间(redirect_url?auth_code=xxx&expires_in=600)
     * @param string $pre_auth_code 预授权码
     * @param bool $isPc 是否PC版
     * @param int $auth_type 1 表示手机端仅展示公众号；2 表示仅展示小程序，3 表示公众号和小程序都展示
     * @param string $biz_appid 指定授权唯一的小程序或公众号 。
     * @param string $category_id_list 指定的权限集 id 列表 如果有多个权限集，则权限集 id 与id之间用中竖线隔开
     * @return bool|string
     */
    public function appidOAuth2($redirectUrl = '', $pre_auth_code = '', $isPc = true, $auth_type = 3, $biz_appid = '', $category_id_list = '')
    {
        if (!$pre_auth_code = $pre_auth_code ? $pre_auth_code : $this->appidPreAuthCodeGet()) {
            return false;
        }

        $url = $isPc ? 'https://mp.weixin.qq.com/cgi-bin/componentloginpage?' : 'https://open.weixin.qq.com/wxaopen/safe/bindcomponent?';
        $url .= ('component_appid=' . $this->component_appid);
        $url .= ('&pre_auth_code=' . $pre_auth_code);
        $url .= ('&redirect_uri=' . urlencode($this->getRedirectUrl($redirectUrl)));
        $url .= ('&auth_type=' . $auth_type);
        $url .= ('&biz_appid=' . $biz_appid);
        $url .= ('&category_id_list=' . $category_id_list);

        return $url;
    }

    /**
     * 获取授权帐号调用令牌
     * @param $authorization_code
     * @return bool|array ['authorization_info' => ['authorizer_appid' => '授权方 appid', 'authorizer_access_token' => '接口调用令牌', 'authorizer_refresh_token' => '刷新令牌', 'expires_in' => 'authorizer_access_token 的有效期', 'func_info' => '授权给开发者的权限集列表']]
     */
    public function appidQueryAuth($authorization_code)
    {
        $data['authorization_code'] = $authorization_code;
        $data['component_appid'] = $this->component_appid;
        if (!$result = $this->httpPost(Urls::AUTH_QUERY_URL, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result;
    }

    /**
     * 拉取已授权的帐号信息
     * @param int $offset 偏移位置/起始位置
     * @param int $count 拉取数量，最大为 500
     * @return bool|array ['total_count' => '', 'list' => ['authorizer_appid', 'refresh_token']]
     */
    public function appidList($offset = 0, $count = 500)
    {
        $data['component_appid'] = $this->component_appid;
        $data['count'] = $count;
        $data['offset'] = $offset;
        if (!$result = $this->httpPost(Urls::AUTH_LIST, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result;
    }

    /**
     * 获取授权帐号详情
     * @see https://developers.weixin.qq.com/doc/oplatform/openApi/OpenApiDoc/authorization-management/getAuthorizerInfo.html
     * @param $authorizer_appid
     * @return bool
     */
    public function appidInfo($authorizer_appid)
    {
        $data['authorizer_appid'] = $authorizer_appid;
        $data['component_appid'] = $this->component_appid;
        if (!$result = $this->httpPost(Urls::AUTH_INFO, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result;
    }

    /**
     * 获取授权方选项信息
     * @param $authorizer_appid
     * @param $option_name
     * @return bool
     */
    public function appidAuthorOptionGet($authorizer_appid, $option_name)
    {
        $data['option_name'] = $option_name;
        $data['authorizer_appid'] = $authorizer_appid;
        $data['component_appid'] = $this->component_appid;

        if (!$result = $this->httpPost(Urls::AUTH_OPTION, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result['option_value'];
    }

    /**
     * 设置授权方选项信息
     * @see https://developers.weixin.qq.com/doc/oplatform/openApi/OpenApiDoc/authorization-management/setAuthorizerOption.html
     * @param $authorizer_appid
     * @param $option_name location_report 地理位置上报选项 voice_recognize 语音识别开关选项 customer_service 多客服开关选项
     * @param $option_value 0 无 1 开
     * @return bool
     */
    public function appidAuthorOptionSet($authorizer_appid, $option_name, $option_value)
    {
        $data['option_name'] = $option_name;
        $data['option_value'] = $option_value;
        $data['authorizer_appid'] = $authorizer_appid;
        $data['component_appid'] = $this->component_appid;

        if (!$result = $this->httpPost(Urls::AUTH_OPTION_SET, $data, $this->useComponentAccessToken())) {
            return false;
        }

        return $result['option_value'];
    }
}
