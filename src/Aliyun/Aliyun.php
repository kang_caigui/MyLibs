<?php

namespace Kangcg\Aliyun;

use Kangcg\Base\Component;
use Kangcg\Aliyun\Extend\V3\Sms;

/**
 * 阿里云
 * Class Aliyun
 * @property string accessKeyId         应用ID 一般不用传递
 * @property string accessSecret        应用ID 一般不用传递
 * @property string host                请求地址--dysmsapi.aliyuncs.com
 * @property string version             2017-05-25
 * @package Kang\Libs\Tencent
 */
class Aliyun extends Component
{
    use Sms;

    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    const HOST = 'dysmsapi.aliyuncs.com';

    const OOS_HOST = 'oss-cn-hangzhou.aliyuncs.com';

    public function getHost()
    {
        return isset($this->_config['host']) ? $this->_config['host'] : self::HOST;
    }

    public function getVersion()
    {
        return isset($this->_config['version']) ? $this->_config['version'] : '2017-05-25';
    }
}
