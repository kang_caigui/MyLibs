<?php

namespace Kangcg\Aliyun\Extend\V3;


trait Sms
{
    use V3;

    public function sendSms(string $phone, string $signName, string $templateCode, array $templateParam)
    {
        $data['SignName'] = $signName;
        $data['TemplateCode'] = $templateCode;
        $data['TemplateParam'] = json_encode($templateParam, JSON_UNESCAPED_UNICODE);
        $data['PhoneNumbers'] = $phone;

        return $this->httpRequest('/', 'SendSms', $data);
    }
}