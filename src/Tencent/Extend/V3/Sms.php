<?php
namespace Kangcg\Tencent\Library\V3;

/**
 * 短信模板发送
 * @property string $smsSdkAppId
 */
trait Sms{
    /**
     * 发送短信
     * @param string|array $phone 下发手机号码
     * @param string $templateId 模板 ID，必须填写已审核通过的模板 ID
     * @param string|array $params 模板参数的个数需要与 TemplateId 对应模板的变量个数保持一致
     * @param string $signName 发送国内短信该参数必填，且需填写签名内容而非签名ID
     * @param string $senderId 国内短信无需填写该项；国际/港澳台短信已申请独立 SenderId 需要填写该字段，默认使用公共 SenderId，无需填写该字段
     * @return false|array
     */
    public function sendSms($phone, $templateId, $params, $signName = null, $senderId = null)
    {
        $data['PhoneNumberSet'] = (array)$phone;
        $data['SmsSdkAppId'] = $this->smsSdkAppId;
        $data['TemplateId'] = $templateId;
        $data['SignName'] = $signName;
        $data['TemplateParamSet'] = (array)$params;
        $data['SenderId'] = $senderId;
        if(!$this->region) $this->region = 'ap-nanjing';

        if (!$result = $this->httpRequest(array_filter($data), self::SMS_URL, __FUNCTION__, '2021-01-11')) {
            return false;
        }

        return $result['Response']['SendStatusSet'];
    }
    /**
     * 发送单条短信
     * @param string|array $phone 下发手机号码
     * @param string $templateId 模板 ID，必须填写已审核通过的模板 ID
     * @param string|array $params 模板参数的个数需要与 TemplateId 对应模板的变量个数保持一致
     * @param string $signName 发送国内短信该参数必填，且需填写签名内容而非签名ID
     * @param string $senderId 国内短信无需填写该项；国际/港澳台短信已申请独立 SenderId 需要填写该字段，默认使用公共 SenderId，无需填写该字段
     * @return bool
     */
    public function sendSmsOne(string $phone, $templateId, $params, $signName = null, $senderId = null)
    {
        if(!$result = $this->sendSms($phone, $templateId, $params, $signName = null, $senderId)){
            return false;
        }

        if($result[0]['Code'] != 'Ok'){
            return $this->setErrors($result[0]['Code'], $result[0]['Message']);
        }

        return true;
    }
}
