<?php

namespace Kangcg\Tencent;

use Kangcg\Base\Component;
use Kangcg\Tencent\Library\V3\Sms;
use Kangcg\Tencent\Library\V3\Ssl;
use Kangcg\Tencent\Library\V3\V3;

/**
 * 腾讯--常用接口 -- v3签名
 * https://ai.qq.com/ 腾讯 API
 * https://ai.qq.com/doc/faceverify.shtml 文档
 * Class Tencent
 * @property string appid         应用ID 一般不用传递
 * @property string secretId     访问密钥 头像->访问管理->API密钥管理
 * @property string secretKey    访问密匙 头像->访问管理->API密钥管理
 * @property string region       地域 ap-guangzhou ap-beijing ap-nanjing
 * @property string $smsSdkAppId 短信应用 SDKAppID 短信->短信应用->应用列表
 * @package Kang\Libs\Tencent
 */
class Tencent extends Component
{
    use Sms;
    use Ssl;
    use V3;

    const SMS_URL = 'https://sms.tencentcloudapi.com';
    const SSL_URL = 'https://ssl.tencentcloudapi.com';

    const CERT_TYPE_NGINX = 'Nginx';
    const CERT_TYPE_APACHE = 'Apache';
    const CERT_TYPE_IIS = 'IIS';
    const CERT_TYPE_TOMCAT = 'Tomcat';
}
