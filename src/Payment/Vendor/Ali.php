<?php

namespace Kangcg\Payment\Vendor;

use Kangcg\Base\Component;
use Kangcg\Helper\Curl;
use Kangcg\Helper\HelperFunc;
use Kangcg\Payment\PaymentInterface;

/**
 * Class Ali
 * @property string $app_id          支付宝分配给开发者的应用ID
 * @property string $public_key    商户公钥，填写对应签名算法类型的私钥，如何生成密钥参考：https://docs.open.alipay.com/291/105971和https://docs.open.alipay.com/200/105310
 * @property string $private_key    商户私钥，填写对应签名算法类型的私钥，如何生成密钥参考：https://docs.open.alipay.com/291/105971和https://docs.open.alipay.com/200/105310
 * @property string $app_cert              应用公钥证
 * @property string $alipay_cert          支付宝公钥证书
 * @property string $alipay__root_cert    支付宝根证书文件
 * @property string $scope    授权模式 auth_base或auth_user。如果只需要获取用户id，填写auth_base即可。如需获取头像、昵称等信息，则填写auth_user
 * @property string $app_auth_token 应用授权
 * @property string $version        调用的接口版本，固定为：1.0
 * @property string $format          仅支持JSON
 * @property string $charset         请求使用的编码格式，如utf-8,gbk,gb2312等
 * @property string $sign_type       商户生成签名字符串所使用的签名算法类型，目前支持RSA2和RSA，推荐使用RSA2
 * @package Kangcg\Payment\Vendor
 */
class Ali extends Component implements PaymentInterface
{
    const VERSION = 2.0;

    const BASE_URL = 'https://openapi.alipay.com/gateway.do?';
    const AUTHORIZE_URL = 'https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?';

    protected $_config = [
        'format' => 'JSON',
        'charset' => 'utf-8',
        'sign_type' => 'RSA2',
        'version' => '1.0',
        'scope' => 'auth_user',
    ];

    /**
     * 授权获取用户信息
     * @param string $redirectUrl 回跳地址
     * @param string $state 而外参数
     * @return array|null
     */
    public function authorize($redirectUrl = '', $state = '')
    {
        //通过code获得access_token和user_id
        if (!isset($_GET['auth_code'])) {
            $url = $this->__CreateOauthUrlForCode(HelperFunc::getRedirectUrl($redirectUrl, true), $state);
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            return $this->doAuthorize($_GET['auth_code']);
        }
    }

    //下单
    public function unified(array $params)
    {
        if (empty($params['method'])) {
            throw new \Exception('请传递支付方式 method！');
        }

        $commonParams = [
            'app_auth_token' => $this->app_auth_token,
            'app_id' => $this->app_id,
            'format' => $this->format,
            'charset' => $this->charset,
            'sign_type' => $this->sign_type,
            'version' => $this->version,
            'notify_url' => $params['notify_url'] ?? null,
            'return_url' => $params['return_url'] ?? null,
            'timestamp' => date('Y-m-d H:i:s'),
            'method' => $params['method'],
        ];

        unset($params['method'], $params['notify_url'], $params['return_url']);

        $commonParams['biz_content'] = json_encode($params, JSON_UNESCAPED_UNICODE);
        $commonParams['sign'] = $this->generateSign($params, $this->sign_type);
        return $this->request(self::BASE_URL, $commonParams);
    }

    //支付通知
    public function notice(\Closure $closure)
    {
        $params = $_POST;
        if (!isset($params['sign']) || !isset($params['sign_type'])) {
            return $this->noticeResponse('FAIL', '非法请求');
        }

        $sign = $params['sign'];
        $signType = $params['sign_type'];

        unset($params['sign_type'], $params['sign']);
        if ($this->verify($this->getSignContent($params), $sign, $signType)) {
            return $closure($params);
        }

        return false;
    }

    protected function noticeResponse($code = 'SUCCESS', $message = 'SUCCESS')
    {
        return json_encode([
            'code' => $code,
            'message' => $message,
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 支付订单查询
     * @param array $params
     * @param string trade_no       二选一
     * @param string out_trade_no   二选一
     * @return array
     */
    public function find($params)
    {
        $commonParams = [
            'app_id' => $this->app_id,
            'method' => 'alipay.trade.query',
            'format' => $this->format,
            'charset' => $this->charset,
            'sign_type' => $this->sign_type,
            'timestamp' => date('Y-m-d H:i:s'),
            'version' => $this->version,
            'app_auth_token' => $this->app_auth_token,
        ];

        $commonParams['biz_content'] = json_encode($params, JSON_UNESCAPED_UNICODE);
        $commonParams['sign'] = $this->generateSign($params, $this->sign_type);
        return $this->request(self::BASE_URL, $commonParams);
    }

    /**
     * 支付订单查询
     * @param array $params
     * @param string trade_no       二选一
     * @param string out_trade_no   二选一
     * @param string operator_id    商家操作员编号 id，由商家自定义
     * @return array
     */
    public function close(array $params)
    {
        $commonParams = [
            'app_id' => $this->app_id,
            'method' => 'alipay.trade.close',
            'format' => $this->format,
            'charset' => $this->charset,
            'sign_type' => $this->sign_type,
            'timestamp' => date('Y-m-d H:i:s'),
            'version' => $this->version,
            'notify_url' => $params['notify_url'] ?? null,
            'app_auth_token' => $this->app_auth_token,
        ];

        $commonParams['biz_content'] = json_encode($params, JSON_UNESCAPED_UNICODE);
        $commonParams['sign'] = $this->generateSign($params, $this->sign_type);
        return $this->request(self::BASE_URL, $commonParams);
    }

    /**
     * 支付订单退款申请
     * @param array $params
     * @param string trade_no       二选一
     * @param string out_trade_no   二选一
     * @param price refund_amount   退款金额，单位元
     * //以下为可选
     * @param string refund_reason   退款原因说明
     * @param string out_request_no  标识一次退款请求，需要保证在交易号下唯一
     * @return array
     */
    public function refund($params)
    {
        $commonParams = [
            'app_id' => $this->app_id,
            'method' => 'alipay.trade.refund',
            'format' => $this->format,
            'charset' => $this->charset,
            'sign_type' => $this->sign_type,
            'timestamp' => date('Y-m-d H:i:s'),
            'version' => $this->version,
            'app_auth_token' => $this->app_auth_token,
        ];

        $commonParams['biz_content'] = json_encode($params, JSON_UNESCAPED_UNICODE);
        $commonParams['sign'] = $this->generateSign($params, $this->sign_type);
        return $this->request(self::BASE_URL, $commonParams);
    }

    /**
     * 支付订单退款查询
     * @param array $params
     * @param string trade_no       二选一
     * @param string out_trade_no   二选一
     * @param string out_request_no  退款请求号
     * @return array
     */
    public function findRefund($params)
    {
        $commonParams = [
            'app_id' => $this->app_id,
            'method' => 'alipay.trade.fastpay.refund.query',
            'format' => $this->format,
            'charset' => $this->charset,
            'sign_type' => $this->sign_type,
            'timestamp' => date('Y-m-d H:i:s'),
            'version' => $this->version,
            'app_auth_token' => $this->app_auth_token,
        ];

        $commonParams['biz_content'] = json_encode($params, JSON_UNESCAPED_UNICODE);
        $commonParams['sign'] = $this->generateSign($params, $this->sign_type);
        return $this->request(self::BASE_URL, $commonParams);
    }

    //退款通知
    public function noticeRefund(\Closure $closure)
    {
        $params = $_POST;
        if (!isset($params['sign']) || !isset($params['sign_type'])) {
            return $this->noticeResponse('FAIL', '非法请求');
        }

        $sign = $params['sign'];
        $signType = $params['sign_type'];

        unset($params['sign_type'], $params['sign']);
        if ($this->verify($this->getSignContent($params), $sign, $signType)) {
            return $closure($params);
        }

        return false;
    }

    //申请交易账单
    public function tradeBill(array $params)
    {

    }

    //申请资金账单
    public function fundFlowBill(array $params)
    {

    }

    /**
     * 获取access_token和user_id
     * @param string $code
     * @return array | null
     */
    public function doAuthorize($code)
    {
        $commonConfigs = [
            //公共参数
            'app_id' => $this->app_id,
            'method' => 'alipay.system.oauth.token',//接口名称
            'format' => 'JSON',
            'charset' => $this->charset,
            'sign_type' => 'RSA2',
            'timestamp' => date('Y-m-d H:i:s'),
            'version' => '1.0',
            'grant_type' => 'authorization_code',
            'code' => $code,
        ];

        $commonConfigs["sign"] = $this->generateSign($commonConfigs, $commonConfigs['sign_type']);

        return $this->request(self::BASE_URL, $commonConfigs);
    }

    public function request($url, $postData)
    {
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }

        $curl = Curl::getInstall();
        $result = $curl->request($url, $postData, Curl::METHOD_POST);

        return $result ? json_decode($result, JSON_UNESCAPED_UNICODE) : null;
    }

    public function generateSign($params, $signType = "RSA")
    {
        return $this->sign($this->getSignContent($params), $signType);
    }

    /**
     * @return float
     */
    public function getVersion()
    {
        return self::VERSION;
    }

    public function amountConvert($amount)
    {
        return intval($amount * 100);
    }

    /**
     * @param $data
     * @param $sign
     * @param string $signType
     * @return bool
     * @throws \Exception
     */
    protected function verify($data, $sign, $signType = 'RSA')
    {
        $pubKey = $this->public_key;
        $res = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($pubKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";

        if (!$res) {
            throw new \Exception('支付宝RSA公钥错误。请检查公钥文件格式是否正确');
        }

        //调用openssl内置方法验签，返回bool值
        if ("RSA2" == $signType) {
            $result = (bool)openssl_verify($data, base64_decode($sign), $res, version_compare(PHP_VERSION, '5.4.0', '<') ? SHA256 : OPENSSL_ALGO_SHA256);
        } else {
            $result = (bool)openssl_verify($data, base64_decode($sign), $res);
        }

        return $result;
    }

    protected function sign($data, $signType = "RSA")
    {
        $priKey = $this->private_key;
        $res = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($priKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

        if (!$res) {
            throw new \Exception('您使用的私钥格式错误，请检查RSA私钥配置');
        }

        if ("RSA2" == $signType) {
            openssl_sign($data, $sign, $res, version_compare(PHP_VERSION, '5.4.0', '<') ? SHA256 : OPENSSL_ALGO_SHA256); //OPENSSL_ALGO_SHA256是php5.4.8以上版本才支持
        } else {
            openssl_sign($data, $sign, $res);
        }

        $sign = base64_encode($sign);
        return $sign;
    }

    protected function getSignContent(array $params)
    {
        ksort($params);
        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;
        return false;
    }

    /**
     * 构造获取token的url连接
     * @param string $redirectUrl 微信服务器回跳的url，需要url编码
     * @return string 返回构造好的url
     */
    private function __CreateOauthUrlForCode($redirectUrl, $state = '')
    {
        $urlObj['app_id'] = $this->app_id;
        $urlObj['redirect_uri'] = $redirectUrl;
        $urlObj['scope'] = $this->scope;
        $urlObj['state'] = $state;
        $bizString = $this->ToUrlParams($urlObj);

        return self::AUTHORIZE_URL . $bizString;
    }

    /**
     * 拼接签名字符串
     * @param array $urlObj
     * @return 返回已经拼接好的字符串
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") $buff .= $k . "=" . $v . "&";
        }

        $buff = trim($buff, "&");
        return $buff;
    }
}
