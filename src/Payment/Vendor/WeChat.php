<?php
declare(strict_types=1);

namespace Kangcg\Payment\Vendor;

use Kangcg\Base\Component;
use Kangcg\Helper\Curl;
use Kangcg\Payment\PaymentException;
use Kangcg\Payment\PaymentInterface;

/**
 * Class WeChat
 * @property string $mchid        微信支付商户号
 * @property string $appid        公众号APPID
 * @property string $secret       APIv3密钥
 * @property string $serial      证书序列号
 * @property string $private     apiclient_key.pem的文件内容，可以先将后缀名改为txt，然后获取里面内容
 * @property string $notify_url  支付异步通知
 */
class WeChat extends Component implements PaymentInterface
{
    const VERSION = 3.0;

    const URL_MAIN = 'https://api.mch.weixin.qq.com';
    const URL_SPARE = 'https://api2.mch.weixin.qq.com';
    const AUTH_TAG_LENGTH_BYTE = 16;

    protected $_url = [
        'jsapi' => '/v3/pay/transactions/jsapi',
        'app' => '/v3/pay/transactions/app',
        'h5' => '/v3/pay/transactions/h5',
        'native' => '/v3/pay/transactions/native',
    ];

    public function unified(array $params)
    {
        if (empty(isset($params['method']))) {
            throw new \Exception('请传递支付方式 method！');
        }

        $method = $params['method'];
        if (!isset($this->_url[$method])) {
            throw new \Exception('支付方式 错误！');
        }

        unset($params['method']);
        $url = self::URL_MAIN . $this->_url[$method];

        $headers = $this->getHeaders($url, $params, 'POST');
        return $this->request($url, $params, $headers);
    }

    //支付通知
    public function notice(\Closure $closure)
    {
        if (!$input = file_get_contents('php://input')) {
            return $this->noticeResponse('FAIL', '非法请求');
        }

        $input = json_decode($input, true);
        if (!isset($input['resource']['associated_data']) || !isset($input['resource']['nonce']) || !isset($input['resource']['ciphertext'])) {
            return $this->noticeResponse('FAIL', '非法请求');
        }

        if (!$result = $this->decryptToString($input['resource']['associated_data'], $input['resource']['nonce'], $input['resource']['ciphertext'])) {
            return $this->noticeResponse('FAIL', '签名错误');
        }

        $result = json_decode($result, true);
        if (isset($result['trade_state']) && $closure) {
            if ($result['trade_state'] == 'SUCCESS' && $closure($result)) {
                return $this->noticeResponse();
            }

            return $this->noticeResponse('FAIL', '处理失败');
        } else {
            return $this->noticeResponse('FAIL', '签名错误');
        }
    }

    protected function noticeResponse($code = 'SUCCESS', $message = 'SUCCESS')
    {
        return json_encode([
            'code' => $code,
            'message' => $message,
        ], JSON_UNESCAPED_UNICODE);
    }

    //订单查询
    public function find($out_trade_no)
    {
        $url = self::URL_MAIN . '/v3/pay/transactions/out-trade-no/' . $out_trade_no . '?mchid=' . $this->mchid;

        return $this->request($url, [], [], Curl::METHOD_GET);
    }

    //订单关闭
    public function close($out_trade_no)
    {
        $url = self::URL_MAIN . "/v3/pay/transactions/out-trade-no/{$out_trade_no}/close";

        return $this->request($url, ['mchid' => $this->mchid]);
    }

    /**
     * @param array $params
     * @param string transaction_id 原支付交易对应的微信订单号，与out_trade_no二选一
     * @param string out_trade_no  原支付交易对应的商户订单号，与transaction_id二选一
     * @param string out_refund_no  商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔
     * @param array amount['refund'] 退款金额,单位为分，只能为整数，不能超过原订单支付金额
     * @param array amount['total']  原支付交易的订单总金额，单位为分，只能为整数。
     * @param array amount['currency']  符合ISO 4217标准的三位字母代码，目前只支持人民币：CNY。
     */
    public function refund($params)
    {
        $url = self::URL_MAIN . '/v3/refund/domestic/refunds';
        return $this->request($url, $params);
    }

    //退款申请查询
    public function findRefund($out_refund_no)
    {
        $url = self::URL_MAIN . '/v3/refund/domestic/refunds/' . $out_refund_no;
        return $this->request($url, [], [], Curl::METHOD_GET);
    }

    //退款通知
    public function noticeRefund(\Closure $closure)
    {
        if (!$input = file_get_contents('php://input')) {
            return $this->noticeResponse('FAIL', '非法请求');
        }

        if ($input['event_type'] != 'REFUND.SUCCESS') {
            return $this->noticeResponse('SUCCESS', '处理成功');
        }

        if (!$closure($input)) {
            return $this->noticeResponse('FAIL', '处理失败');
        }

        return $this->noticeResponse('SUCCESS', '处理成功');
    }

    /**
     * 申请交易账单
     * @param array $params
     * @param bill_date 账单日期，格式yyyy-MM-DD，仅支持三个月内的账单下载申请。
     * @param tar_type  压缩类型，不填则以不压缩的方式返回数据流 可选取值：GZIP
     * @param bill_type 账单类型，不填则默认是ALL
     * ALL: 返回当日所有订单信息（不含充值退款订单）
     * SUCCESS: 返回当日成功支付的订单（不含充值退款订单）
     * REFUND: 返回当日退款订单（不含充值退款订单）
     * RECHARGE_REFUND: 返回当日充值退款订单
     * ALL_SPECIAL: 返回个性化账单当日所有订单信息
     * SUC_SPECIAL: 返回个性化账单当日成功支付的订单
     * REF_SPECIAL: 返回个性化账单当日退款订单
     */
    public function tradeBill(array $params)
    {
        $url = self::URL_MAIN . '/v3/bill/tradebill?' . http_build_query($params);
        return $this->request($url, [], [], Curl::METHOD_GET);
    }

    /**
     * 申请资金账单
     * @param array $params
     * @param bill_date 账单日期，格式yyyy-MM-DD，仅支持三个月内的账单下载申请。
     * @param account_type  资金账户类型，不填默认是BASIC
     * BASIC: 基本账户
     * OPERATION: 运营账户
     * FEES: 手续费账户
     * ALL: 所有账户（该枚举值只限电商平台下载二级商户资金流水账单场景使用）
     */
    public function fundFlowBill(array $params)
    {
        $url = self::URL_MAIN . '/v3/bill/fundflowbill?' . http_build_query($params);
        return $this->request($url, [], [], Curl::METHOD_GET);
    }

    public function request($url, $params = [], $headers = [], $method = 'POST')
    {
        $curl = Curl::getInstall();
        $curl->setHeader($headers);
        $params = is_array($params) ? json_encode($params, JSON_UNESCAPED_UNICODE) : $params;
        if (!$result = $curl->request($url, $params, $method)) {
            return false;
        }

        return json_decode($result, true);
    }

    public function getAuthStr($requestUrl, $reqParams = []): string
    {
        return $this->getSchema() . ' ' . $this->getToken($requestUrl, $reqParams);
    }

    protected function getHeaders($url, $params, $method): array
    {
        $headers['Authorization'] = $this->getToken($url, $params, $method);
        $headers['Accept'] = 'application/json';
        $headers['Content-Type'] = 'application/json';
        $headers['User-Agent'] = $_SERVER['HTTP_USER_AGENT'];

        return $headers;
    }

    private function getToken($requestUrl, array $reqParams = [], $method = 'POST'): string
    {
        $body = $reqParams ? json_encode($reqParams, JSON_UNESCAPED_UNICODE) : '';
        $nonce = $this->getNonce();
        $timestamp = time();
        $message = $this->buildMessage($nonce, $timestamp, $requestUrl, $body, $method);
        $sign = $this->sign($message);
        return sprintf('mchid="%s",nonce_str="%s",timestamp="%d",serial_no="%s",signature="%s"',
            $this->mchid, $nonce, $timestamp, $this->serial, $sign
        );
    }

    private function getNonce()
    {
        static $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    private function buildMessage($nonce, $timestamp, $requestUrl, $body = '', $method = 'POST'): string
    {
        $urlParts = parse_url($requestUrl);
        $canonicalUrl = ($urlParts['path'] . (!empty($urlParts['query']) ? "?{$urlParts['query']}" : ""));
        return strtoupper($method) . "\n" .
            $canonicalUrl . "\n" .
            $timestamp . "\n" .
            $nonce . "\n" .
            $body . "\n";
    }

    private function sign($message): string
    {
        if (!in_array('sha256WithRSAEncryption', openssl_get_md_methods(true))) {
            throw new PaymentException("当前PHP环境不支持SHA256withRSA");
        }

        $priv_key_id = $this->getPrivateKey($this->private);
        if (!openssl_sign($message, $sign, $priv_key_id, 'sha256WithRSAEncryption')) {
            throw new PaymentException("签名验证过程发生了错误");
        }

        return base64_encode($sign);
    }

    private function getSchema(): string
    {
        return 'WECHATPAY2-SHA256-RSA2048';
    }

    private function getPublicKey($publicKey)
    {
        $publicKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($publicKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";

        $publicKey = openssl_pkey_get_public($publicKey);
        return $publicKey;
    }

    private function getPrivateKey($privateKey)
    {
        try {
            $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
                wordwrap($privateKey, 64, "\n", true) .
                "\n-----END RSA PRIVATE KEY-----";

            $privateKey = openssl_pkey_get_private($privateKey);
        } catch (\Exception $exception) {
            throw new PaymentException("私钥证书获取失败！");
        }

        return $privateKey;
    }

    /**
     * Decrypt AEAD_AES_256_GCM ciphertext
     * @param string $associatedData AES GCM additional authentication data
     * @param string $nonceStr AES GCM nonce
     * @param string $ciphertext AES GCM cipher text
     *
     * @return string|bool      Decrypted string on success or FALSE on failure
     */
    private function decryptToString(string $associatedData, string $nonceStr, string $ciphertext)
    {
        $ciphertext = \base64_decode($ciphertext);
        if (strlen($ciphertext) <= self::AUTH_TAG_LENGTH_BYTE) {
            return false;
        }

        $ctext = substr($ciphertext, 0, -self::AUTH_TAG_LENGTH_BYTE);
        $authTag = substr($ciphertext, -self::AUTH_TAG_LENGTH_BYTE);

        return \openssl_decrypt($ctext, 'aes-256-gcm', $this->secret, \OPENSSL_RAW_DATA, $nonceStr,
            $authTag, $associatedData);
    }

    public function amountConvert($amount)
    {
        return intval($amount * 100);
    }

    public function getVersion()
    {
        return self::VERSION;
    }
}
