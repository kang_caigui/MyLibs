<?php

namespace Kangcg\Payment;

interface PaymentInterface
{
    public function __construct(array $config);

    //下单
    public function unified(array $params);

    /**
     * 支付通知
     * @param \Closure $closure return bool
     * @return mixed
     */
    public function notice(\Closure $closure);

    /**
     * 订单查询
     * @param $params
     * @return mixed
     */
    public function find($params);

    //订单关闭
    public function close(array $params);

    //退款申请
    public function refund($params);

    //退款申请查询
    public function findRefund($params);

    //退款通知
    public function noticeRefund(\Closure $closure);

    //申请交易账单
    public function tradeBill(array $params);

    //申请资金账单
    public function fundFlowBill(array $params);

    //获取接口版本
    public function getVersion();
    //金额转换
    public function amountConvert($amount);
}
